### 1.1.0 (December 9, 2022)

* Merge upstream `h5bp/server-configs-apache` v6.0.0
  [https://github.com/h5bp/server-configs-apache/releases/tag/6.0.0]
* Add new `Cache-Control` boilerplate to `wpbp_htaccess.conf` and rebuild `.wpbp_htaccess`

### 1.0.0 (September 22, 2022)
