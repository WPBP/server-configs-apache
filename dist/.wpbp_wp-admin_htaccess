# Apache Server Configs v6.0.0 | MIT License
# https://github.com/h5bp/server-configs-apache

# (!) Using `.htaccess` files slows down Apache, therefore, if you have
#     access to the main server configuration file (which is usually
#     called `httpd.conf`), you should add this logic there.
#
#     https://httpd.apache.org/docs/current/howto/htaccess.html

# ######################################################################
# # WP-ADMIN DIRECTORY                                                 #
# ######################################################################

# ----------------------------------------------------------------------
# | Password protection                                                |
# ----------------------------------------------------------------------

# Restrict access to the `wp-admin` directory with HTTP Basic Authentication.
#
# Password protecting the `wp-admin` directory and WordPress `Dashboard Screen`
# can greatly improve the security of this most critical directory, and
# important entry point to your website.
#
# (!) Do not enable this and the `Restrict access by IP address` rules together;
#     the resulting conflicts will causes faults/errors. If both are needed,
#     enable the `Password protection and IP address restrictions combined`
#     rules instead.

# (1) Set a custom error message.
#
# (2) Allow access to admin-ajax.php and the js/password-strength-meter.min.js.
#
#     Some plugins require Ajax and/or password strength meter functionality in
#     WordPress. For these to function, anonymous access must be granted to
#     these files.
#
#     `Files` based directives also provided. If preferred to the default
#     ``FilesMatch` approach, just comment that section out and uncomment the
#     alternative.
#
#     (!) The Apache compatible `LiteSpeed Web Server` (LSWS) is known to lack
#         compatibility with certain Apache 2.4 directives. Although not noted
#         in the wiki link, LSWS simply ignores/silently fails on a number of
#         2.4 access control directives, in certain scenarios. Thus the rule
#         tests for the `LiteSpeed` module and uses Apache 2.2 style directives
#         for LSWS:
#         https://www.litespeedtech.com/support/wiki/doku.php/litespeed_wiki:config:unsupported_apache_modules_by_lsws
#         https://www.litespeedtech.com/support/forum/threads/htaccess-files-not-being-used.20371/
#         https://stackoverflow.com/questions/70557867/litespeed-enterprise-not-obeying-htaccess-require-rules
#         https://httpd.apache.org/docs/2.2/howto/access.html
#
# (3) Set the directives necessary to implement HTTP basic authentication on the
#     wp-admin directory.
#
# (!) Update the `AuthName` directive to customise the text presented as part
#     of the password dialog box. And update `AuthUserFile` directive to set the
#     path to the password file.
#
#     The generate a password file using the `htpasswd` CLI or a reputable
#     online generator, and upload the file to your server:
#     https://httpd.apache.org/docs/current/programs/htpasswd.html
#     https://www.askapache.com/online-tools/htpasswd-generator/
#
# (!) Store your htpasswd file below the `public_html` directory and outside of
#     any other publically accessible directory, with appropriate file ownership
#     and access permissions.
#
# https://httpd.apache.org/docs/current/howto/auth.html
# https://wordpress.org/support/article/htaccess/#password-protect-login
# https://www.wpwhitesecurity.com/securing-wordpress-wp-admin-htaccess/
# https://www.wpbeginner.com/wp-tutorials/how-to-password-protect-your-wordpress-admin-wp-admin-directory/

# (1)
# ErrorDocument 401 "Unauthorized - User authentication required to access this resource"

# (2)
# <IfModule mod_authz_core.c>

#     <FilesMatch "(^admin-ajax\.php|password-strength-meter\.min\.js)$">
#         <IfModule !LiteSpeed>
#             Require all granted
#          </IfModule>
#         <IfModule LiteSpeed>
#             Order allow,deny
#             Allow from all
#             Satisfy any
#         </IfModule>
#     </FilesMatch>

#     <Files "admin-ajax.php">
#         <IfModule !LiteSpeed>
#             Require all granted
#         </IfModule>
#         <IfModule LiteSpeed>
#             Order allow,deny
#             Allow from all
#             Satisfy any
#         </IfModule>
#     </Files>
#
#     <Files "password-strength-meter.min.js">
#         <IfModule !LiteSpeed>
#             Require all granted
#         </IfModule>
#         <IfModule LiteSpeed>
#             Order allow,deny
#             Allow from all
#             Satisfy any
#         </IfModule>
#     </Files>

# </IfModule>

# (3)
# AuthType Basic
# AuthBasicProvider file
# AuthUserFile /full/path/to/.htpasswd
# AuthName "Password protected"
# require valid-user

# ----------------------------------------------------------------------
# | Restrict access by IP address                                      |
# ----------------------------------------------------------------------

# Only allow access to the `wp-admin` directory from certain IP addresses.
#
# Restricting `wp-admin` directory access to connections originating from the
# local host and remote clients accessing the WordPress `Dashboard Screen`
# (i.e. Administrators, Editors, etc.) can greatly reduce the range of IP's from
# which this most critical directory, and important entry point to your website,
# can be attacked from.
#
# Websites best suited for this have a very few users, connecting from a limited
# range of IP addresses. The most practicable implementation would be to monitor
# your public IP address and once the total range of subnets available to you is
# known, specify those as partial IP addresses:
# https://www.askapache.com/online-tools/whoami/
#
# (!) Do not enable this and the `Password protection` rules together; the
#     resulting conflicts will causes faults/errors. If both are needed, enable
#     the `Password protection and IP address restrictions combined` rules
#     instead.

# (1) Set a custom error message.
#
# (2) Update the `Require ip` directive with your IP address/es, seperated by
#     spaces. IPv4 full and partial IP addresses, network/netmask pair's, and/or
#     network/nnn CIDR specifications can be specified. IPv6 addresses and
#     subnets can be specified. Refer to the Apache `mod_authz_host`
#     documentation for more details:
#     https://httpd.apache.org/docs/current/mod/mod_authz_host.html#requiredirectives
#
#     (!) The Apache compatible `LiteSpeed Web Server` (LSWS) is known to lack
#         compatibility with certain Apache 2.4 directives. Although not noted
#         in the wiki link, LSWS simply ignores/silently fails on a number of
#         2.4 access control directives, such as `Require ip`, <RequireAll> and
#         <RequireAny>. Thus the rule tests for the `LiteSpeed` module and uses
#         Apache 2.2 style directives for LSWS. So if your web server is running
#         LSWS, update the `Allow from` directive:
#         https://www.litespeedtech.com/support/wiki/doku.php/litespeed_wiki:config:unsupported_apache_modules_by_lsws
#         https://www.litespeedtech.com/support/forum/threads/htaccess-files-not-being-used.20371/
#         https://stackoverflow.com/questions/70557867/litespeed-enterprise-not-obeying-htaccess-require-rules
#         https://httpd.apache.org/docs/2.2/howto/access.html
#
#
# (!) If your WordPress Dashboard Screen is suddenly returning 403 Forbidden
#     errors, your IP address has likely changed and is not covered by the below
#     rule. FTP into your server to update the 'Require ip` directive with the
#     new address, or comment out the rule entirely to restore access.
#
# https://httpd.apache.org/docs/current/howto/access.html
# https://htaccessbook.com/access-control-apache-2-4/
# https://wordpress.org/support/article/htaccess/#require-specific-ip

# (1)
# ErrorDocument 403 "Forbidden - You don't have permission to access this resource"

# (2)
# <IfModule mod_authz_core.c>
#     <IfModule !LiteSpeed>
#         <RequireAll>
#             Require all denied
#             Require local
#             Require ip 10 172.20 192.168.2
#         </RequireAll>
#     </IfModule>
#     <IfModule LiteSpeed>
#         Order deny,allow
#         Deny from all
#         Allow from 127.0.0.1, 10.*.*.*, 172.20.*.*, 192.168.2.*
#     </IfModule>
# </IfModule>

# ----------------------------------------------------------------------
# | Password protection and IP address restrictions combined           |
# ----------------------------------------------------------------------

# This combines the `Restrict access by IP address` and `Password protection`
# configuration snippets into a single set of Apache 2.4 compatible rules.
#
# Use this if you require access to the `wp-admin` directory to be restricted
# both by IP address and with HTTP Basic Authentication.

# (1) Set custom error messages.
#
# (2) Allow access to admin-ajax.php and the js/password-strength-meter.min.js.
#
#     Some plugins require Ajax and/or password strength meter functionality in
#     WordPress. For these to function, anonymous access must be granted to
#     these files.
#
#     `Files` based directives also provided. If preferred to the default
#     ``FilesMatch` approach, just comment that section out and uncomment the
#     alternative.
#
# (3) Set the directives necessary to implement HTTP basic authentication on the
#     wp-admin directory.
#
# (!) Update the `AuthName` directive to customise the text presented as part
#     of the password dialog box. And update `AuthUserFile` directive to set the
#     path to the password file.
#
#     The generate a password file using the `htpasswd` CLI or a reputable
#     online generator, and upload the file to your server:
#     https://httpd.apache.org/docs/current/programs/htpasswd.html
#     https://www.askapache.com/online-tools/htpasswd-generator/
#
#     And update the `Require ip` directive with your IP address/es, seperated by
#     spaces. IPv4 full and partial IP addresses, network/netmask pair's, and/or
#     network/nnn CIDR specifications can be specified. IPv6 addresses and
#     subnets can be specified. Refer to the Apache `mod_authz_host`
#     documentation for more details:
#     https://httpd.apache.org/docs/current/mod/mod_authz_host.html#requiredirectives
#
#     (!) The Apache compatible `LiteSpeed Web Server` (LSWS) is known to lack
#         compatibility with certain Apache 2.4 directives. Although not noted
#         in the wiki link, LSWS simply ignores/silently fails on a number of
#         2.4 access control directives, such as `Require ip`, <RequireAll> and
#         <RequireAny>. Thus the rule tests for the `LiteSpeed` module and uses
#         Apache 2.2 style directives for LSWS. So if your web server is running
#         LSWS, update the `Allow from` directive:
#         https://www.litespeedtech.com/support/wiki/doku.php/litespeed_wiki:config:unsupported_apache_modules_by_lsws
#         https://www.litespeedtech.com/support/forum/threads/htaccess-files-not-being-used.20371/
#         https://stackoverflow.com/questions/70557867/litespeed-enterprise-not-obeying-htaccess-require-rules
#         https://httpd.apache.org/docs/2.2/howto/access.html
#
# (!) Store your htpasswd file below the `public_html` directory and outside of
#     any other publically accessible directory, with appropriate file ownership
#     and access permissions.
#
# https://httpd.apache.org/docs/current/howto/auth.html
# https://wordpress.org/support/article/htaccess/#password-protect-login
# https://www.wpwhitesecurity.com/securing-wordpress-wp-admin-htaccess/
# https://www.wpbeginner.com/wp-tutorials/how-to-password-protect-your-wordpress-admin-wp-admin-directory/

# (1)
# ErrorDocument 401 "Unauthorized - User authentication required to access this resource"
# ErrorDocument 403 "Forbidden - You don't have permission to access this resource"

# (2)
# <IfModule mod_authz_core.c>

#     <FilesMatch "(^admin-ajax\.php|password-strength-meter\.min\.js)$">
#         <IfModule !LiteSpeed>
#             Require all granted
#          </IfModule>
#         <IfModule LiteSpeed>
#             Order allow,deny
#             Allow from all
#             Satisfy any
#         </IfModule>
#     </FilesMatch>

#     <Files "admin-ajax.php">
#         <IfModule !LiteSpeed>
#             Require all granted
#         </IfModule>
#         <IfModule LiteSpeed>
#             Order allow,deny
#             Allow from all
#             Satisfy any
#         </IfModule>
#     </Files>
#
#     <Files "password-strength-meter.min.js">
#         <IfModule !LiteSpeed>
#             Require all granted
#         </IfModule>
#         <IfModule LiteSpeed>
#             Order allow,deny
#             Allow from all
#             Satisfy any
#         </IfModule>
#     </Files>

# </IfModule>

# (3)
# <IfModule mod_authz_core.c>
#     AuthType Basic
#     AuthBasicProvider file
#     AuthUserFile /full/path/to/.htpasswd
#     AuthName "Password protected"
#     <IfModule !LiteSpeed>
#         <RequireAll>
#             <RequireAny>
#                 Require local
#                 Require ip 10 172.20 192.168.2
#             </RequireAny>
#             Require valid-user
#         </RequireAll>
#     </IfModule>
#     <IfModule LiteSpeed>
#         Order deny,allow
#         Deny from all
#         Allow from 127.0.0.1, 10.*.*.*, 172.20.*.*, 192.168.2.*
#         Require valid-user
#     </IfModule>
# </IfModule>

