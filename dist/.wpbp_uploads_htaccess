# Apache Server Configs v6.0.0 | MIT License
# https://github.com/h5bp/server-configs-apache

# (!) Using `.htaccess` files slows down Apache, therefore, if you have
#     access to the main server configuration file (which is usually
#     called `httpd.conf`), you should add this logic there.
#
#     https://httpd.apache.org/docs/current/howto/htaccess.html

# ######################################################################
# # UPLOADS DIRECTORY                                                  #
# ######################################################################

# ----------------------------------------------------------------------
# | Prevent script execution                                           |
# ----------------------------------------------------------------------

# Add scripts to the `cgi-script` handler and then remove CGI script execution.
#
# The `AddHandler` directive registers numerous script file extensions to the
# `cgi-script` handler. Then the `Options` directive removes `-` the ability to
# execute CGI scripts `ExecCGI` from this directory. Any attempts to access or
# execute these files will result in a 403 Forbidden error, reducing the threat
# of scripted attacks against this often targeted directory.
#
# (!) For various reasons, PHP has used several different file extensions over
#     the years (e.g. PHP 1 was *.php, PHP 2 *.phtml, PHP 3 *.php3, and >= PHP 4
#     back to *.php, however many handlers continued to execute `versioned`
#     extenions (.php4|5|6|7|8)). Depending on how the PHP handler is configured
#     on your server, any number of the PHP extensions may be executed.
#
# (!) Security plugins may provide an option to enable this directive, like the
#     Wordfence `Disable Code Execution for Uploads directory` option. Be sure
#     you are not needlessly duplicating functions before enabling this section:
#     https://www.wordfence.com/help/dashboard/options/#exec-uploads
#
# https://codex.wordpress.org/htaccess%20for%20subdirectories#Preventing_Script_Execution
# https://wordpress.org/support/article/htaccess/#options
# https://httpd.apache.org/docs/trunk/mod/mod_cgi.html
# https://docs.cpanel.net/ea4/php/php-handlers/

AddHandler cgi-script .php .php2 .php3 .php4 .php5 .php6 .php7 .php8 .pht .phtml .phps .phar .pl .py .jsp .asp .htm .html .shtml .sh .cgi
Options -ExecCGI

# ----------------------------------------------------------------------
# | Restrict file access                                               |
# ----------------------------------------------------------------------

# Grant and/or deny access to specific file types.
#
# By only allowing file access for extensions which can be uploaded to and/or
# served from your `wp-content/uploads` directory, attempts to access any other
# files will result in a 403 Forbidden error. This can reduce the threat of
# malicious file injections or backdoor exploits targeting this directory.
#
# (!) NEVER USE BOTH RULES AT THE SAME TIME!

# (1) Grant access to specific file types and deny access to all other files.
#     This is the most secure option, but also the most restrictive on what
#     files can be accessed.
#
#     Regex matches:
#     .avif .avifs .bmp .css .cur .gif .ico .jpg .jpeg .jxl .pdf .png .apng .svg
#     .svgz .tif .tiff .webp
#
#     (!) Ensure you know what file types are being uploaded to and/or served
#         from your `wp-content/uploads` directory and any subdirectories
#         (Plugins may also serve .css, .pdf, or other files from here). Then
#         update the `<FilesMatch>` regular expression with only the file
#         extensions that are applicable to your website.
#
#     (!) Note that `<Files>` and `<FilesMatch>` refer to actual files, not user
#         requests, meaning the case of file extensions must be considered. So
#         if you uses all UPPERCASE file extenions, then uncomment and use the
#         second `<FilesMatch>` statement. Or if you have any combination of
#         UPPER/lower/MiXeD case extensions, then use the third one. In either
#         case, please also take a long hard look in the mirror! :-)
#
#     (!)  Use of Scalable Vector Graphics (SVG) comes with serious security
#          concerns that should be addressed before using them on your website:
#          https://www.wpbeginner.com/wp-tutorials/how-to-add-svg-in-wordpress/
#          https://www.bjornjohansen.com/svg-in-wordpress
#
# (2) Deny access to specific files (scripts). All other files can be accessed.
#     Use this if option (1) causes untraceable or unfixable errors. Although it
#     is the less secure option, it still provides reasonable security, whilst
#     being far more permissive on what files can be accessed.
#
#     Regex matches with case insensitive `(?i)` mode modifier:
#     .php .php2 .php3 .php4 .php5 .php6 .php7 .php8 .pht .phtml .phps .phar .pl
#     .py .jsp .asp .htm .html .shtml .sh .cgi
#
# https://digwp.com/2012/09/secure-media-uploads/
# https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types

# (1)
<IfModule mod_authz_core.c>
    <Files ~ ".*\..*">
        Require all denied
    </Files>
    <FilesMatch "\.(avifs?|bmp|css|cur|gif|ico|jpe?g|jxl|pdf|a?png|svgz?|tiff?|webp)$">
    # <FilesMatch "\.(AVIFS?|BMP|CSS|CUR|GIF|ICO|JPE?G|JXL|PDF|A?PNG|SVGZ?|TIFF?|WEBP)$">
    # <FilesMatch "(?i)\.(avifs?|bmp|css|cur|gif|ico|jpe?g|jxl|pdf|a?png|svgz?|tiff?|webp)$">
        Require all granted
    </FilesMatch>
</IfModule>

# (2)
# <IfModule mod_authz_core.c>
#     <FilesMatch "(?i)\.(ph(p[2345678]?|t[^ml]?|ps|ar)|p[ly]|[aj]sp|s?html?|sh|cgi)$">
#         Require all denied
#     </Files>
# </IfModule>

# ----------------------------------------------------------------------
# | Script document type                                               |
# ----------------------------------------------------------------------

# Force `text/plain` document type for scripts.
#
# This directive forces the specified script extensions to be used as the
# text/plain document type, overriding all handlers and actions otherwise
# associated with them.
#
# Regex matches with case insensitive `(?i)` mode modifier:
# .php .php2 .php3 .php4 .php5 .php6 .php7 .php8 .pht .phtml .phps .phar .pl .py
# .jsp .asp .htm .html .shtml .sh .cgi
#
# (!) This will output the content of matching files to the display. Be certain
#     affected directories do not contain files that may expose sensitive data
#     (e.g. database credentials, secret keys, or closed source scripts). To
#     prevent this and instead return a 403 Forbidden error, also enable the
#     `Restrict file access` section.
#
# https://codex.wordpress.org/htaccess%20for%20subdirectories#Preventing_Script_Execution

# <FilesMatch "(?i)\.(ph(p[2345678]?|t[^ml]?|ps|ar)|p[ly]|[aj]sp|s?html?|sh|cgi)$">
#     ForceType text/plain
# </FilesMatch>

# ----------------------------------------------------------------------
# | Disable PHP engine                                                 |
# ----------------------------------------------------------------------

# Set the PHP `engine` configuration directive to off `0`.
#
# This prevents the execution of any PHP script within the directory. Used in
# isolation the error message `Forbidden: PHP engine is disable.` is returned
# with a 403 Forbidden status code. If the `Script document type` section is
# also enabled, the contents of php files are displayed. If the `Restrict file
# access` section is also enabled, a 403 Forbidden error is returned.
#
# (!) Security plugins may provide an option to enable this directive, like the
#     Wordfence `Disable Code Execution for Uploads directory` option. Be sure
#     you are not needlessly duplicating functions before enabling this section:
#     https://www.wordfence.com/help/dashboard/options/#exec-uploads
#
# https://www.wpwhitesecurity.com/php-hardening-wordpress/#block-php-execution
# https://endurtech.com/how-to-prevent-scripts-and-code-from-executing-within-your-wordpress-uploads-directory/

# <IfModule mod_php5.c>
#     php_flag engine 0
# </IfModule>
# <IfModule mod_php7.c>
#     php_flag engine 0
# </IfModule>
# <IfModule mod_php.c>
#     php_flag engine 0
# </IfModule>

