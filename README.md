# [WPBP Apache Server Configs](https://codeberg.org/wpbp/server-configs-apache)

It's the **H5BP Apache Server Configs... WordPress-ified!**

WordPress Boilerplate (WPBP) Apache Server Configs is a clone of the HTML5 Boilerplate ([H5BP](https://github.com/h5bp)) [Apache Server Configs](https://github.com/h5bp/server-configs-apache), adding a collection of configuration snippets that can help your server improve your WordPress websites performance and security.


## Getting Started

This project is simply the H5BP Apache Server Configs, with:

 * custom modules/configuration snippets (`h5bp/wpbp/...`),
 * custom .htaccess configuration files (`bin/wpbp*`), and
 * ready-to-use .htaccess files, built using the above (`dist/.wpbp*`).

All of which can be used to produce the .htaccess files for a WordPress website. Specifically:

 * `/.htaccess` (document root),
 * `wp-admin/.htaccess`, and
 * `wp-content/uploads/.htaccess`.

Meaning this project is intended for the generation of `.htacess` file based Apache server configs, for a standalone WordPress website. It does not support:

 * the creation of a [main server configuration
  file](https://httpd.apache.org/docs/current/configuring.html#main)
  (usually called `httpd.conf`),
 * installation via [npm](https://www.npmjs.com/), nor
 * WordPress Multisites, subdirectory based installations, or any other weird setups. 8-P

So if you're using good old `famous 5 minute install` WordPress on a shared host, then this is probably for you!

The only changes to the H5BP Apache Server Configs are the renaming of the readme ([H5BP_README.md](H5BP_README.md)) and changelog ([H5BP_CHANGELOG.md](H5BP_CHANGELOG.md)) files, and an updated [LICENSE.txt](LICENSE.txt) file. Everything else is vanilla H5BP Apache Server Configs, and this is maintained as a remote "upstream" dependancy.

Otherwise, please read the original [H5BP_README.md](H5BP_README.md) for full details, and in particular the **.htaccess file** section - that's essentially how to use WPBP Apache Server Configs, and there's no point repeating it here! :-)


### Basic structure

This repository has the following structure:

```text
./
├── bin/
│   ├── wpbp_htaccess.conf
│   ├── wpbp_uploads_htaccess.conf
│   └── wpbp_wp-admin_htaccess.conf
├── dist/
│   ├── .wpbp_htaccess
│   ├── .wpbp_uploads_htaccess
│   └── .wpbp_wp-admin_htaccess
└── h5bp/
    └── wpbp/
        ├── wp_core/
        │   ├── basic_wp.conf
        └── .../
```

* **`bin/wpbp*`**

  Configuration files defining which modules to enable or disable for the `.htaccess` files of your WordPress website. The default values should be adjusted according to your specific needs, and your own custom modules/code snippets added as necessary:

  * **`wpbp_htaccess.conf`**

    The .htaccess configuration file for the document root (a.k.a. web root or website root directory). This file contains all H5BP modules with thier default values (as per `bin/htaccess.conf`), adding WordPress specific modules from `h5bp/wpbp`.

  * **`wpbp_uploads_htaccess.conf`**

    The .htaccess configuration file for the `uploads` directory. This contains all of the `h5bp/wpbp/wp-content/uploads` modules.

  * **`wpbp_wp-admin_htaccess.conf`**

    The .htaccess configuration file for the `wp-admin` directory. This contains all of the `h5bp/wpbp/wp-admin` modules.

* **`dist/.wpbp*`**

  Ready-to-use .htaccess files for your WordPress website, built according to the configuration files definitions. Each file should be renamed to `.htaccess` before being uploaded to their respective locations:

  * **`.wpbp_htaccess`**

    The .htaccess file for your WordPress websites document root (a.k.a. web root or website root directory); `/var/www/html/.htaccess` or `/home/user_name/public_html/.htaccess` or possibly another path depending on your host.

  * **`.wpbp_uploads_htaccess`**

    The .htaccess file for your WordPress websites `uploads` directory; `wp-content/uploads/.htaccess`

  * **`.wpbp_wp-admin_htaccess`**

    The .htaccess file for your WordPress websites `wp-admin` directory; `wp-admin/.htaccess`

* **`h5bp/wpbp`**

  All individual config snippets (mixins) for WordPress, to be included as desired. The `wp-admin` and `wp-content/uploads` directories contain snippets speicifically for the `.htaccess` files in each of these directories. All other directories contain snippets for the document root `.htaccess` file. Your own custom modules/code snippets can be added as necessary.


## Support

* Apache v**2.4.10**+


## Contributing

Anyone is welcome to contribute, just be cool! :-)

* [Issues](https://codeberg.org/WPBP/server-configs-apache/issues)
* [Pull Requests](https://codeberg.org/WPBP/server-configs-apache/pulls)

**Please note:** Only contributions specifically relating to WPBP [Apache Server Configs](https://codeberg.org/WPBP/server-configs-apache) will be considered; that is anything under the [Basic structure](#basic-structure). Feature requests or personal support requests are not accepted.


## Acknowledgements

WPBP [Apache Server Configs](https://codeberg.org/WPBP/server-configs-apache) is only possible thanks to:

 * Everybody and anybody, that ever contributed, in any way,
 * All those good people sharing their knowledge of WordPress/Apache (links in each WPBP configuration snippet),
 * And all the awesome [contributors](https://github.com/h5bp/server-configs-apache/graphs/contributors) of the upstream H5BP [Apache Server Configs](https://github.com/h5bp/server-configs-apache/)!


## License

The code is available under the [MIT license](LICENSE.txt).
