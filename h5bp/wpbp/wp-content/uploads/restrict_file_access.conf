# ----------------------------------------------------------------------
# | Restrict file access                                               |
# ----------------------------------------------------------------------

# Grant and/or deny access to specific file types.
#
# By only allowing file access for extensions which can be uploaded to and/or
# served from your `wp-content/uploads` directory, attempts to access any other
# files will result in a 403 Forbidden error. This can reduce the threat of
# malicious file injections or backdoor exploits targeting this directory.
#
# (!) NEVER USE BOTH RULES AT THE SAME TIME!

# (1) Grant access to specific file types and deny access to all other files.
#     This is the most secure option, but also the most restrictive on what
#     files can be accessed.
#
#     Regex matches:
#     .avif .avifs .bmp .css .cur .gif .ico .jpg .jpeg .jxl .pdf .png .apng .svg
#     .svgz .tif .tiff .webp
#
#     (!) Ensure you know what file types are being uploaded to and/or served
#         from your `wp-content/uploads` directory and any subdirectories
#         (Plugins may also serve .css, .pdf, or other files from here). Then
#         update the `<FilesMatch>` regular expression with only the file
#         extensions that are applicable to your website.
#
#     (!) Note that `<Files>` and `<FilesMatch>` refer to actual files, not user
#         requests, meaning the case of file extensions must be considered. So
#         if you uses all UPPERCASE file extenions, then uncomment and use the
#         second `<FilesMatch>` statement. Or if you have any combination of
#         UPPER/lower/MiXeD case extensions, then use the third one. In either
#         case, please also take a long hard look in the mirror! :-)
#
#     (!)  Use of Scalable Vector Graphics (SVG) comes with serious security
#          concerns that should be addressed before using them on your website:
#          https://www.wpbeginner.com/wp-tutorials/how-to-add-svg-in-wordpress/
#          https://www.bjornjohansen.com/svg-in-wordpress
#
# (2) Deny access to specific files (scripts). All other files can be accessed.
#     Use this if option (1) causes untraceable or unfixable errors. Although it
#     is the less secure option, it still provides reasonable security, whilst
#     being far more permissive on what files can be accessed.
#
#     Regex matches with case insensitive `(?i)` mode modifier:
#     .php .php2 .php3 .php4 .php5 .php6 .php7 .php8 .pht .phtml .phps .phar .pl
#     .py .jsp .asp .htm .html .shtml .sh .cgi
#
# https://digwp.com/2012/09/secure-media-uploads/
# https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types

# (1)
<IfModule mod_authz_core.c>
    <Files ~ ".*\..*">
        Require all denied
    </Files>
    <FilesMatch "\.(avifs?|bmp|css|cur|gif|ico|jpe?g|jxl|pdf|a?png|svgz?|tiff?|webp)$">
    # <FilesMatch "\.(AVIFS?|BMP|CSS|CUR|GIF|ICO|JPE?G|JXL|PDF|A?PNG|SVGZ?|TIFF?|WEBP)$">
    # <FilesMatch "(?i)\.(avifs?|bmp|css|cur|gif|ico|jpe?g|jxl|pdf|a?png|svgz?|tiff?|webp)$">
        Require all granted
    </FilesMatch>
</IfModule>

# (2)
# <IfModule mod_authz_core.c>
#     <FilesMatch "(?i)\.(ph(p[2345678]?|t[^ml]?|ps|ar)|p[ly]|[aj]sp|s?html?|sh|cgi)$">
#         Require all denied
#     </Files>
# </IfModule>
